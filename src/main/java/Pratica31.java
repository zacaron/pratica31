
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author zacaron
 */
public class Pratica31 {
    
    private static Date inicio = new Date();
    private static String meuNome = "Alexandro Marcelo Zacaron";
    private static String s1;
    private static char ch1, ch2;
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1981, Calendar.JUNE, 26);
    private static Date dataNasc;
    private static long ms1, ms2, ms3, ms4;
    private static Date fim = new Date();
    public static void main(String[] args) {
        //Nome em letra MAIÚSCULA               
        System.out.println("Nome: "+meuNome.toUpperCase());
        
        //Nome formatado
        s1 = meuNome.substring(17);
        ch1 = meuNome.charAt(0);
        ch2 = meuNome.charAt(10);
        System.out.println(s1+", "+ch1+". "+ch2+".");
        
        //idade
        dataNasc = dataNascimento.getTime();
        ms1 = dataNasc.getTime();
        ms2 = inicio.getTime();
        System.out.println("Número de dias decorridos até hoje "+((ms2-ms1)/86400000));
        
        //Contador tempo execução
        ms3 = inicio.getTime();
        ms4 = fim.getTime();
        System.out.println("Tempo decorrido de processamento, em milissegundos "+ (ms4-ms3));
        
    }
    
}
